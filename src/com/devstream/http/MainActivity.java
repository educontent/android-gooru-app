
package com.devstream.http;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {
	String username,password;

	public  HashMap<String, String> hm=new HashMap<String, String>();
public static String title,collectionItemId;



	public ArrayList<String> listItems = new ArrayList<String>();   
	 ListView list; 
	 private ArrayAdapter<String> adapter;
	
	private static final String TAG = "MainActivity";
	 private JSONArray jarr;
	JSONObject o;
	private static final String URL = "http://concept.goorulearning.org/gooruapi/rest/v2/account/login?apiKey=cba20746-ec45-457a-8387-421a0cdf51c7";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		JSONObject jsonObjSend = new JSONObject();
		Intent recieve=getIntent();
		username=recieve.getExtras().getString("username");
		password=recieve.getExtras().getString("password");

		try {
			jsonObjSend.put("username", username);
			jsonObjSend.put("password", password);
		
		} catch (JSONException e) {
			e.printStackTrace();
		}

			JSONObject jsonObjRecv = httpClient.SendHttpPost(URL, jsonObjSend);
		try {
//			System.out.println(jsonObjRecv.get("token"));
			 JSONObject	user = (JSONObject) jsonObjRecv.get("user");
				String	userID=(String) user.get("partyUid");
				final String workspaceUrl="http://concept.goorulearning.org/gooruapi/rest/v2/collection/"+userID+"/workspace?sessionToken=99282f30-a8e7-11e3-94c4-12313924c4da";
				

 			HttpClient client =new DefaultHttpClient();  

 			HttpGet request = new HttpGet();

 			request.setURI(new URI(workspaceUrl));

 			HttpResponse response = client.execute(request);
 			String jsonString = EntityUtils.toString(response.getEntity());
				jarr=new JSONArray(jsonString);
				System.out.println(jarr.length());

				 for(int i=0;i<jarr.length();i++)
				 {
					JSONObject ob=(JSONObject)jarr.get(i);
					
				
					JSONObject resource=(JSONObject)ob.get("resource");
					collectionItemId=resource.get("gooruOid").toString();
					title=resource.get("title").toString();
					hm.put(title, collectionItemId);
					
					
				
			}
				 Iterator it = hm.entrySet().iterator();
				    while (it.hasNext()) {
				        Map.Entry pairs = (Map.Entry)it.next();
				        listItems.add((String) pairs.getKey());
				       
				    }
				  	list=(ListView)findViewById(R.id.listView1);
					adapter=new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,listItems);
					list.setAdapter(adapter);
					list.setOnItemClickListener(new OnItemClickListener() {

			          
						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
								long arg3) {
							// TODO Auto-generated method stub
							System.out.println(hm.get(list.getItemAtPosition(arg2)));
							Intent i = new Intent(MainActivity.this, webView.class);
							i.putExtra("collecId", hm.get(list.getItemAtPosition(arg2)));
							   	startActivity(i);
						}

			        });
					
			
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		 catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
}