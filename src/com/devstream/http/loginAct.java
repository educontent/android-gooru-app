package com.devstream.http;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class loginAct extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		Button b1=(Button)findViewById(R.id.button1);
		b1.setOnClickListener(new View.OnClickListener() {
			
		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EditText userName=(EditText)findViewById(R.id.editText1);
				EditText pwd=(EditText)findViewById(R.id.editText2);
				String username=userName.getText().toString();
				String password=pwd.getText().toString();
				Intent in=new Intent(loginAct.this,MainActivity.class);
				in.putExtra("username", username);
				in.putExtra("password", password);
				startActivity(in);
				
			}
		});
		
	}

}
